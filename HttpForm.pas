unit HttpForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent, IdTCPServer, IdHTTPServer,
  StdCtrls, StrUtils, IdThreadMgr, IdThreadMgrDefault, IdCustomHTTPServer, IdSocketHandle;

type
  TForm1 = class(TForm)
    IdHTTPServer1: TIdHTTPServer;
    ListBox1: TListBox;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    procedure IdHTTPServer1CommandGet(AThread: TIdPeerThread;
      RequestInfo: TIdHTTPRequestInfo; ResponseInfo: TIdHTTPResponseInfo);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses IdHTTPHeaderInfo;

{$R *.dfm}

procedure TForm1.IdHTTPServer1CommandGet(AThread: TIdPeerThread;
  RequestInfo: TIdHTTPRequestInfo; ResponseInfo: TIdHTTPResponseInfo);
var
  HtmlResult: string;
  i: Integer;
begin
  // log
  Listbox1.Items.Add(FormatDateTime('yyyy-mm-dd hh:mm:zzz', now) + ' - ' +
    RequestInfo.RemoteIP + ' - ' + RequestInfo.Document);
  // respond


  HtmlResult := '<h1>Mini HttpServer</h1>' +
    '<p>Your IP:' + RequestInfo.RemoteIP + '</p><hr>' +
    '<p>Request: ' + RequestInfo.Document + '</p>' +
    '<p>Host: ' + RequestInfo.Host + '</p>' +
    '<p>Params: ' + RequestInfo.UnparsedParams + '</p>' +
    '<p>The headers of the request follow: </p>';
  for i := 0 to RequestInfo.RawHeaders.Count - 1 do
    HtmlResult := HtmlResult + RequestInfo.RawHeaders[i] + '<br>';

  HtmlResult := '<!DOCTYPE html>' + #13#10 +
    '<html>' + #13#10 +
    '  <head>' + #13#10 +
    '    <meta charset="windows-1253">' + #13#10 +
    '    <title>Mini HttpServer</title>' + #13#10 +
    '  </head>' + #13#10 +
    '  <body>' + #13#10 +
    HtmlResult +
    '  </body>' + #13#10 +
    '</html>';


  ResponseInfo.ContentText := HtmlResult;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  SHandle: TIdSocketHandle;
begin

  with IdHTTPServer1 do
  begin
    //DefaultPort := StrToInt( Edit1.Text);
    Active := False;
    Bindings.Clear; //make sure there's no other bindings
    SHandle := Bindings.Add;
    SHandle.IP := '0.0.0.0';
    SHandle.Port := StrToInt(Edit1.Text);
    Active := True;
    Listbox1.Items.Add('Listening on port ' + Edit1.Text);
  end;
end;
//------------------------------------------------------------------------------

procedure TForm1.Button2Click(Sender: TObject);
begin
  IdHTTPServer1.Active := False;
  Listbox1.Items.Add('Stopped.');
end;
//------------------------------------------------------------------------------
end.

